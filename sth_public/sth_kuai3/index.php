<?php
    include "../frame/__config.php";
    include "../frame/__data.php";
    include "__data.php";
    $type="pc";
?>
<!doctype html>
<html lang="zh-TW">
<head>
    <?php meta();?>
</head>
<body>
    <div class="site-outter">
        <?php include "../module_component/_site_ad_top.php";?>
        <?php include "../module_header/_pc_header_kuai3.php";?>
         <div class="pc_index_full_menu">
            <div class="pc_index_full_menu_left">       
                <nav class="nav-menu">
                    <a href=""><h2 class="title">全站商品分類</h2></a>
                    <ul class="menu-one">
                        <li>
                            <h3><a href="category.php" class="hot_icon">促銷優惠<img src="../c/img/sth_kuai3/00_18x18.gif"/></a></h3>
                            <div class="menu-expand">
                                <ul class="menu-sub-expand">
                                    <li>
                                        <a href="category.php" target="_blank">電視.4K電視</a>
                                    </li>
                                    <li><a href="#" target="_blank">家庭劇院</a></li>
                                    <li><a href="#" target="_blank">音響.黑膠商品</a></li>
                                    <li><a href="#" target="_blank">藍芽喇叭.揚聲器</a></li>
                                    <li><a href="#" target="_blank">錄音筆.MP3.MP4</a></li>
                                    <li><a href="#" target="_blank">遊戲.玩具.點數卡</a></li>
                                    <li><a href="#" target="_blank">VR/AR相關裝置</a></li>
                                    <li><a href="#" target="_blank">投影機.週邊</a></li>
                                    <li><a href="#" target="_blank">口譯機.樂器.點讀筆</a></li>
                                    <li><a href="#" target="_blank">耳機.喇叭.麥克風</a></li>
                                </ul>
                                
                                <ul class="menu-sub-expand-blue">
                                    <li>
                                        <a href="#">活動專區</a>
                                        <a href="#">品牌活動專區</a>
                                        <a href="#">超夯功能專區</a>
                                        <a href="#" class="bold">品牌專區</a>
                                        <a href="#">最佳觀賞距離</a>
                                        <a href="#">找到好價格</a>
                                        <a href="#">尺寸分類</a>
                                        <a href="#" class="menu-sub-expand-more">more</a>
                                    </li>
                                    <li>
                                        <a href="#">新物、超值專區</a>
                                        <a href="#">館長推薦</a>
                                        <a href="#">Nintendo專區</a>
                                        <a href="#">SONY PS4專區</a>
                                        <a href="#">XBOX ONE專區</a>
                                        <a href="#">遊戲點數卡</a>
                                        <a href="#">遙控智能</a>
                                        <a href="#" class="menu-sub-expand-more">more</a>
                                    </li>
                                    <li>
                                        <a href="#">最佳觀賞距離</a>
                                        <a href="#">找到好價格</a>
                                        <a href="#">尺寸分類</a>
                                    </li>
                                    <li>
                                        <a href="#">新物、超值專區</a>
                                        <a href="#">館長推薦</a>
                                        <a href="#">Nintendo專區</a>
                                        <a href="#">SONY PS4專區</a>
                                        <a href="#">XBOX ONE專區</a>
                                        <a href="#">遊戲點數卡</a>
                                        <a href="#">遙控智能</a>
                                        <a href="#" class="menu-sub-expand-more">more</a>
                                    </li>
                                    <li>
                                        <a href="#">★活動專區★</a>
                                        <a href="#">品牌活動專區</a>
                                        <a href="#">超夯功能專區</a>
                                        <a href="#">品牌專區</a>
                                        <a href="#">最佳觀賞距離</a>
                                        <a href="#">找到好價格</a>
                                        <a href="#">尺寸分類</a>
                                        <a href="#" class="menu-sub-expand-more">more</a>
                                    </li>
                                    <li>
                                        <a href="#">新物、超值專區</a>
                                        <a href="#">館長推薦</a>
                                        <a href="#">Nintendo專區</a>
                                        <a href="#">SONY PS4專區</a>
                                        <a href="#">XBOX ONE專區</a>
                                        <a href="#">遊戲點數卡</a>
                                        <a href="#">遙控智能</a>
                                        <a href="#" class="menu-sub-expand-more">more</a>
                                    </li>
                                    <li>
                                        <a href="#">新物、超值專區</a>
                                        <a href="#">館長推薦</a>
                                        <a href="#">Nintendo專區</a>
                                        <a href="#">SONY PS4專區</a>
                                        <a href="#">XBOX ONE專區</a>
                                        <a href="#">遊戲點數卡</a>
                                        <a href="#">遙控智能</a>
                                        <a href="#" class="menu-sub-expand-more">more</a>
                                    </li>
                                    <li>
                                        <a href="#">活動專區</a>
                                        <a href="#">品牌活動專區</a>
                                        <a href="#">超夯功能專區</a>
                                        <a href="#" class="bold">品牌專區</a>
                                        <a href="#">最佳觀賞距離</a>
                                        <a href="#">找到好價格</a>
                                        <a href="#">尺寸分類</a>
                                        <a href="#" class="menu-sub-expand-more">more</a>
                                    </li>
                                    <li>
                                        <a href="#">活動專區</a>
                                        <a href="#">品牌活動專區</a>
                                        <a href="#">超夯功能專區</a>
                                        <a href="#" class="bold">品牌專區</a>
                                        <a href="#">最佳觀賞距離</a>
                                        <a href="#">找到好價格</a>
                                        <a href="#">尺寸分類</a>
                                        <a href="#" class="menu-sub-expand-more">more</a>
                                    </li>
                                    <li>
                                        <a href="#">新物、超值專區</a>
                                        <a href="#">館長推薦</a>
                                        <a href="#">Nintendo專區</a>
                                        <a href="#">SONY PS4專區</a>
                                        <a href="#">XBOX ONE專區</a>
                                        <a href="#">遊戲點數卡</a>
                                        <a href="#">遙控智能</a>
                                        <a href="#" class="menu-sub-expand-more">more</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li><h3><a href="category.php" class="clean_icon">電視.音響.遊戲<img src="../c/img/sth_kuai3/01_18x18.gif"/></a></h3></li>
                        <li><h3><a href="category.php" class="face_icon">主機.螢幕.零組件<img src="../c/img/sth_kuai3/02_18x18.gif"/></a></h3></li>
                        <li><h3><a href="category.php" class="makeup_icon">電腦週邊.辦公設備<img src="../c/img/sth_kuai3/03_18x18.gif"/></a></h3></li>
                        <li><h3><a href="category.php" class="makeup2_icon">行動通訊.穿戴裝置<img src="../c/img/sth_kuai3/04_18x18.gif"/></a></h3></li>
                        <li><h3><a href="category.php" class="wash_icon">休閒.攝影.車用<img src="../c/img/sth_kuai3/05_18x18.gif"/></a></h3></li>
                        <li><h3><a href="category.php" class="body_icon">筆記型電腦.APPLE<img src="../c/img/sth_kuai3/06_18x18.gif"/></a></h3></li>
                        <li><h3><a href="category.php" class="jp_icon">大型家電.季節專區<img src="../c/img/sth_kuai3/07_18x18.gif"/></a></h3></li>
                        <li><h3><a href="category.php" class="idol_icon">廚房.生活家電<img src="../c/img/sth_kuai3/08_18x18.gif"/></a></h3></li>
                        <li><h3><a href="category.php" class="open_icon">主題旗艦館<img src="../c/img/sth_kuai3/09_18x18.gif"/></a></h3></li>
                        <li><h3><a href="category.php" class="hair_icon">福利品清倉<img src="../c/img/sth_kuai3/09_18x18.gif"/></a></h3></li>
                    </ul>           
                </nav>
            </div>
            <div class="pc_index_full_menu_right clearfix">
                <ul class="multicolor">
                    <li><a href="#"><img src="../c/img/sth_kuai3/$_18x18.gif" alt=""/>&nbsp;現正熱賣</a></li>
                    <li><a href="#"><img src="../c/img/sth_kuai3/$_18x18.gif" alt=""/>&nbsp;福利品清倉</a></li>
                    <li><a href="#"><img src="../c/img/sth_kuai3/$_18x18.gif" alt=""/>&nbsp;家電團購</a></li>
                    <li><a href="#"><img src="../c/img/sth_kuai3/$_18x18.gif" alt=""/>&nbsp;最新降價</a></li>
                </ul>

                <ul class="member">
                    <li><a href="#"><img src="../c/img/sth_kuai3/$_18x18.gif" alt=""/>聯名卡優惠</a></li>
                    <li><a href="#"><img src="../c/img/sth_kuai3/$_18x18.gif" alt=""/>燦坤會員關卡</a></li>
                    <li><a href="#"><img src="../c/img/sth_kuai3/$_18x18.gif" alt=""/>電器醫院</a></li>
                    <li><a href="#"><img src="../c/img/sth_kuai3/$_18x18.gif" alt=""/>會員回娘家  </a></li>
                </ul>
            </div>
        </div>
        
        <div class="site-full-banner" style="background:#c69961;"><!-- 輪播背景色  -->
            <div class="pc_index_full">
                <!-- 輪播  -->
                <div class="swiper-container swiper-container-horizontal swiper-container-autoheight">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide"><img src="../c/img/sth_kuai3/sample/1220x500_01.jpg" alt=""></div>
                        <div class="swiper-slide"><img src="../c/img/sth_kuai3/sample/1220x500_02.jpg" alt=""></div>
                        <div class="swiper-slide"><img src="../c/img/sth_kuai3/sample/1220x500_01.jpg" alt=""></div>
                    </div>

                    <div class="swiper-btn-block">
                        <div class="swiper-btn-limit">
                            <!-- 底部按鈕 -->
                            <div class="swiper-pagination swiper-pagination-clickable swiper-pagination-bullets"></div>
                            <!-- 左右按鈕 -->
                            <div class="swiperBtn swiper-button-prev swiper-button-white"></div>
                            <div class="swiperBtn swiper-button-next swiper-button-white"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="site-body">
            <aside class="site-left">
                <?php include "../module_left/_pc_left_004.php"; ?>
                <?php include "../module_left/_pc_left_013.php";?>
                <?php include "../module_left/_pc_left_005.php";?>
            </aside>
            <article class="site-main">
                <?php include "../module_category/_pc_category_1big_6small_list.php";?>
                <?php include "../module_category/_pc_category_limited_sale_list.php";?>
                <?php include "../module_index/_pc_index_006.php";?>
                <?php include "../module_index/_pc_hot_product.php";?>
                <?php include "../module_index/_pc_video_3block.php";?>

                <?php include "../module_category/_pc_category_1big_6small_list.php";?>
                <?php include "../module_category/_pc_category_1big_6small_list.php";?>
                <?php include "../module_category/_pc_category_1big_6small_list.php";?>
                <?php include "../module_category/_pc_category_1big_6small_list.php";?>
                <?php include "../module_category/_pc_category_hot_products_3list.php";?>
            </article>
            <!-- <aside class="site-right">
                右欄預留空間
            </aside> -->
        </div>
        <div class="nav-to-top">
            <i class="fa fa-angle-double-up"></i>
        </div>
        <?php include "../module_footer/_pc_footer_001.php";?>
        <?php include "../module_footer/_pc_copyright_001.php";?>
    </div>
    <?php include "../frame/__pc_js_library.php";?>
</body>
</html>